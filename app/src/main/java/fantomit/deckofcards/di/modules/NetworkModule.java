package fantomit.deckofcards.di.modules;

import com.google.gson.annotations.SerializedName;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import fantomit.deckofcards.BuildConfig;
import fantomit.deckofcards.webservices.DeckApi;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
@Singleton
public class NetworkModule {

    @Provides
    @Singleton
    GsonConverterFactory providesGsonFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    @Singleton
    OkHttpClient providesOKClient() {
        return new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    Retrofit providesRestAdapter(OkHttpClient client, GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
    }

    @Provides
    @Singleton
    DeckApi providesDeckApi(Retrofit retrofit){
        return retrofit.create(DeckApi.class);
    }
}