package fantomit.deckofcards.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import fantomit.deckofcards.DeckApplication;
import fantomit.deckofcards.utils.ConnectivityUtil;

@Module
@Singleton
public class UtilityModule {
    @Singleton
    @Provides
    ConnectivityUtil providesConnectivityUtil(DeckApplication application){
        return new ConnectivityUtil(application.getApplicationContext());
    }
}