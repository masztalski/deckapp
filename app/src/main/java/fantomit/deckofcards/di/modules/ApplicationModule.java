package fantomit.deckofcards.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import fantomit.deckofcards.DeckApplication;

@Singleton
@Module
public class ApplicationModule {
    DeckApplication app;

    public ApplicationModule(DeckApplication app) {
        this.app = app;
    }

    @Provides
    @Singleton
    DeckApplication providesApplication() {
        return app;
    }

}
