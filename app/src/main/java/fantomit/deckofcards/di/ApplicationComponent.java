package fantomit.deckofcards.di;

import javax.inject.Singleton;

import dagger.Component;
import fantomit.deckofcards.activities.DeckActivity;
import fantomit.deckofcards.di.modules.ApplicationModule;
import fantomit.deckofcards.di.modules.NetworkModule;
import fantomit.deckofcards.di.modules.UtilityModule;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class, UtilityModule.class})
public interface ApplicationComponent {
    void inject(DeckActivity activity);
}
