package fantomit.deckofcards.utils;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ThreadsTransformer<T>{

    public static <T> Observable<T> newThreadToMainThread(Observable<T> tObservable) {
        return tObservable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread());
    }

    public static <T> Observable<T> newThreadToNewThread(Observable<T> tObservable) {
        Scheduler newThread = Schedulers.newThread();
        return tObservable
                .observeOn(newThread)
                .subscribeOn(newThread);
    }
}
