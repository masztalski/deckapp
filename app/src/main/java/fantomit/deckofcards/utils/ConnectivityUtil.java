package fantomit.deckofcards.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectivityUtil {
    private ConnectivityManager mConnectivityManager;

    public ConnectivityUtil(Context context) {
        mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public boolean isNetworkAvailable() {
        NetworkInfo nInfo = mConnectivityManager.getActiveNetworkInfo();
        return nInfo != null && nInfo.isConnectedOrConnecting();
    }
}
