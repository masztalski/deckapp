package fantomit.deckofcards.models;

import com.google.gson.annotations.SerializedName;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Card {
    public static final String KROL = "KING";
    public static final String WALET = "JACK";
    public static final String DAMA = "QUEEN";
    public static final String ACE = "ACE";

    @Retention(RetentionPolicy.RUNTIME)
    @StringDef({TREFL, WINO, SERCA, DZWONKI})
    public @interface Colors {
    }

    public static final String TREFL = "CLUBS";
    public static final String WINO = "SPADES";
    public static final String SERCA = "HEARTS";
    public static final String DZWONKI = "DIAMONDS";


    @SerializedName("image")
    private String mImgUrl;
    @SerializedName("value")
    private String mCardValue;  //@Figures lub 1-10
    @SerializedName("suit")
    @Colors
    private String mSuit;
    @SerializedName("code")
    private String mCardCode;

    public Card(String imgUrl, String cardValue, String suit, String cardCode) {
        mImgUrl = imgUrl;
        mCardValue = cardValue;
        mSuit = suit;
        mCardCode = cardCode;
    }

    public String getImgUrl() {
        return mImgUrl;
    }

    public String getCardValue() {
        return mCardValue;
    }

    public String getSuit() {
        return mSuit;
    }

    public String getCardCode() {
        return mCardCode;
    }

    public void setCardValue(String cardValue) {
        mCardValue = cardValue;
    }
}
