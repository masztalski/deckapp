package fantomit.deckofcards.models;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

public class Combination {
    @Retention(RetentionPolicy.SOURCE)
    @StringDef({KOLOR, SCHODKI, FIGURY, BLIZNIAKI})
    public @interface RodzajeKombinacji {
    }

    public static final String KOLOR = "kolory: ";
    public static final String SCHODKI = "schodki: ";
    public static final String FIGURY = "figury: ";
    public static final String BLIZNIAKI = "bličniaki: ";

    @RodzajeKombinacji
    private String mNazwa;
    private List<Card> mKarty;

    public Combination(String nazwa, List<Card> kodyKart) {
        mNazwa = nazwa;
        mKarty = kodyKart;
    }

    public String getNazwa() {
        return mNazwa;
    }

    public List<Card> getKarty() {
        return mKarty;
    }
}
