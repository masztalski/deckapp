package fantomit.deckofcards;

import com.facebook.stetho.Stetho;

import android.app.Application;

import fantomit.deckofcards.di.ApplicationComponent;
import fantomit.deckofcards.di.DaggerApplicationComponent;
import fantomit.deckofcards.di.modules.ApplicationModule;


public class DeckApplication extends Application {
    public ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        Stetho.initializeWithDefaults(this);

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }
}
