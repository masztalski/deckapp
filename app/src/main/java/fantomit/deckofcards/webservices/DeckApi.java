package fantomit.deckofcards.webservices;

import fantomit.deckofcards.webservices.responses.CardsResponse;
import fantomit.deckofcards.webservices.responses.DeckResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DeckApi {
    @GET("deck/new/shuffle/")
    Observable<DeckResponse> getDeck(@Query("deck_count") int deckCount);
    @GET("deck/{deck_id}/draw/")
    Observable<CardsResponse> drawCards(@Path("deck_id") String deckId, @Query("count") int cardsCount);
    @GET("deck/{deck_id}/shuffle")
    Observable<DeckResponse> reshuffleDeck(@Path("deck_id") String deckId);
}
