package fantomit.deckofcards.webservices.responses;

class BaseResponse {
    private boolean success;
    private String deck_id;
    private int remaining;

    public BaseResponse(boolean success, String deck_id, int remaining) {
        this.success = success;
        this.deck_id = deck_id;
        this.remaining = remaining;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getDeck_id() {
        return deck_id;
    }

    public int getRemaining() {
        return remaining;
    }
}
