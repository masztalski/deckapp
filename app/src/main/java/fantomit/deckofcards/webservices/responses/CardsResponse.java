package fantomit.deckofcards.webservices.responses;

import java.util.List;

import fantomit.deckofcards.models.Card;

public class CardsResponse extends BaseResponse {
    private List<Card> cards;

    public CardsResponse(boolean success, String deck_id, int remaining, List<Card> cards) {
        super(success, deck_id, remaining);
        this.cards = cards;
    }

    public List<Card> getCards() {
        return cards;
    }
}
