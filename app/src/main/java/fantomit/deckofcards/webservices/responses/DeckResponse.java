package fantomit.deckofcards.webservices.responses;

public class DeckResponse extends BaseResponse {
    private boolean shuffled;

    public DeckResponse(boolean success, String deck_id, int remaining, boolean shuffled) {
        super(success, deck_id, remaining);
        this.shuffled = shuffled;
    }

    public boolean isShuffled() {
        return shuffled;
    }
}
