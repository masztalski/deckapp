package fantomit.deckofcards.activities;

interface DeckInterface {
    void showToast(String message);
    void showSpinner();
    void hideSpinner();
}
