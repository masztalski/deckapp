package fantomit.deckofcards.activities;

import com.bumptech.glide.Glide;
import com.jakewharton.rxbinding.widget.RxSeekBar;

import android.content.pm.ActivityInfo;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import fantomit.deckofcards.DeckApplication;
import fantomit.deckofcards.R;
import fantomit.deckofcards.databinding.ActivityMainBinding;
import fantomit.deckofcards.models.Card;
import io.reactivex.disposables.CompositeDisposable;

/*
Przyj�te za�o�enia:
- u�ytkownik na ekranie widzi zawsze tylko 5 kart
- suwak SeekBar s�u�y do ustawienia ilo�ci talii (1-5). W przypadku brak ustawie� pobierana jest 1 talia
- Przycisk Przygotuj Tali� (@id/prepareDeck) wymaga po��czenia z internetem. Przygotowuje zadan� ilo�� talii.
- Przycisk Dobierz nowe karty (@id/drawNewCards) dobiera karty: w przypadku dost�pnego decku online z decku online,w przeciwnym wypadku korzysta z lokalnego decku.
odk�ada zu�yte karty na discard lokalny, wy�wietla nowe oraz dost�pne w�r�d nich kombinacje w formie tekstowej
- Lokalny deck zosta� zdefiniowany, poniewa� API obs�uguje Pile jedynie dla 1 talii.
 */
public class DeckActivity extends AppCompatActivity implements DeckInterface {
    @Inject
    DeckController mController;

    private ActivityMainBinding mBinding;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    /*
    onCreate inicjalizuje:
    - Dependency Injection
    - Data Binding dla powi�zanego widoku
    - listener zmian w SeekBarze
    - subskrypcje dla nowych kart i kombinacji z controllera
    */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        ((DeckApplication) getApplication())
                .mApplicationComponent
                .inject(this);

        mController.onCreate(this);
        hideSpinner();

        //Dla przejrzysto�ci kodu u�ywam biblioteki RxBinding
        RxSeekBar.userChanges(mBinding.deckCount).subscribe(integer -> {
            if (integer > 0) {
                mController.setDeckCount(integer);
                mBinding.deckCountView.setText(String.valueOf(integer));
            }
        });

        mCompositeDisposable.add(mController.subscribeToNewCard(this::loadCards));

        mCompositeDisposable.add(mController.subscribeToCombinations(combinationsList -> {
            String combinationsText = mController.getCombinationsText(combinationsList);
            mBinding.combinations.setText(combinationsText.isEmpty() ? getString(R.string.no_combinations) : combinationsText);
        }));
    }

    /*
    Pobiera obrazki kart i wy�wietla je na ekranie
    Dla wygody i czytelno�ci u�ywam Glide, kt�ry zapewnia cachewoanie pobranej tre�ci i zarz�dzanie w�tkami
     */
    private void loadCards(List<Card> cardsList) {
        //display cards on Screen
        if (cardsList == null) return;

        int index = 0;
        for (Card card : cardsList) {
            switch (index) {
                case 0:
                    Glide.with(this).load(card.getImgUrl()).into(mBinding.card1);
                    break;
                case 1:
                    Glide.with(this).load(card.getImgUrl()).into(mBinding.card2);
                    break;
                case 2:
                    Glide.with(this).load(card.getImgUrl()).into(mBinding.card3);
                    break;
                case 3:
                    Glide.with(this).load(card.getImgUrl()).into(mBinding.card4);
                    break;
                case 4:
                    Glide.with(this).load(card.getImgUrl()).into(mBinding.card5);
                    break;
            }
            index++;
        }
        hideSpinner();
    }

    /*
    onClickListener dla przycisku @id/drawNewCards
     */
    public void drawNewCards(View v) {
        showSpinner();
        mController.getCardsFromDeck();
    }

    /*
    onClickListener dla przycisku @id/prepareDeck
     */
    public void initDeck(View v) {
        showSpinner();
        mController.initCardSet();
    }

    /**
     * utility do sterowania wy�wietlaniem toast�w spoza activity
     * @param message wiadomo�� do wy�wietlenia w toa�cie
     */
    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    /*
    Na wyj�ciu z activity wyrejestrowywane s� subskrypcje oraz data binding
     */
    @Override
    protected void onDestroy() {
        mCompositeDisposable.dispose();
        mBinding.unbind();
        mController.onDestroy();
        super.onDestroy();
    }

    /**
     * Metoda do pokazywania spinnera �adowania danych
     */
    @Override
    public void showSpinner() {
        mBinding.progressBar.setProgressVisibe(true);
        mBinding.progressBar.setmProgressTitle("Trwa �adowanie danych");
        mBinding.progressBar.setmShowBlur(true);
    }

    /**
     * Metoda do ukrywania spinnera �adowania danych
     */
    @Override
    public void hideSpinner() {
        mBinding.progressBar.setProgressVisibe(false);
    }
}
