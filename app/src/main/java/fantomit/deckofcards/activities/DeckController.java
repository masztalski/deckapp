package fantomit.deckofcards.activities;

import com.annimon.stream.Stream;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import fantomit.deckofcards.models.Card;
import fantomit.deckofcards.models.Combination;
import fantomit.deckofcards.utils.ConnectivityUtil;
import fantomit.deckofcards.utils.ThreadsTransformer;
import fantomit.deckofcards.webservices.DeckApi;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class DeckController {
    private static final int CARDS_COUNT = 5;
    private static final int MIN_CARDS_FOR_SET = 3;

    @Inject
    DeckApi mDeckApi;
    @Inject
    ConnectivityUtil mConnectivityUtil;

    private DeckInterface mView;

    private String mDeckId = null;
    private int mDeckCount = 1;

    private List<Card> mDrawnCards = new ArrayList<>(); //5 wyci�gni�tych kart
    private List<Card> mDiscardedCards = new ArrayList<>(); //karty odrzucone
    private List<Card> mReshuffledDeck = new ArrayList<>(); //nowa talia utworzona z odrzuconych; Ze wzgl�du na dopuszczalne u�ycie wi�cej ni� 1 talii niemo�liwe jest skorzystanie z funkcji Pile dost�pnej w API.

    private List<Combination> mFoundedCombinations = new ArrayList<>();

    private PublishSubject<List<Card>> mDrawnCardsSubject = PublishSubject.create();
    private PublishSubject<List<Combination>> mCombinationsSubject = PublishSubject.create();

    private int mRemaingCardsOnline = -1;

    @Inject
    DeckController() {
    }

    void onCreate(DeckInterface deckView) {
        mView = deckView;
    }

    void onDestroy() {
        mView = null;
    }

    void setDeckCount(int deckCount) {
        mDeckCount = deckCount;
    }

    void initCardSet() {
        mDiscardedCards.clear();
        mReshuffledDeck.clear();

        if (!mConnectivityUtil.isNetworkAvailable()) {
            if (isViewPresent()) mView.showToast("Brak internetu");
            return;
        }

        mDeckApi.getDeck(mDeckCount).compose(ThreadsTransformer::newThreadToMainThread).subscribe(deckResponse -> {
            if (deckResponse.isSuccess() && deckResponse.isShuffled()) {
                mDeckId = deckResponse.getDeck_id();
                mRemaingCardsOnline = deckResponse.getRemaining();
                Log.d("Controller", "Received deck of " + deckResponse.getRemaining() + " card");
                getCardsFromDeck();
            } else if (isViewPresent()) {
                mView.showToast("Wyst�pi� b��d podczas pobierania talii. Spr�buj ponownie");
            }
        }, Throwable::printStackTrace);
    }

    void getCardsFromDeck() {
        if (mDeckId != null) {
            if (!mConnectivityUtil.isNetworkAvailable()) {
                if (isViewPresent()) mView.showToast("Brak internetu");
                if (!mReshuffledDeck.isEmpty() && mRemaingCardsOnline == 0) {
                    if (isViewPresent())
                        mView.showToast("Deck online wyczerpany. Korzystam z decku offline");
                    drawFromLocalDeck();
                } else {
                    mView.hideSpinner();
                }
                return;
            }

            mDeckApi.drawCards(mDeckId, CARDS_COUNT).compose(ThreadsTransformer::newThreadToMainThread).subscribe(cardsResponse -> {

                mDiscardedCards.addAll(mDrawnCards); //odk�adam karty na stos odrzuconych
                mDrawnCards.clear(); //czyszcz� stos dostepnych
                Log.d("Controller", "Drawn cards cleared");
                Log.d("Controller", "Cards in discarded deck: " + mDiscardedCards.size());

                if (cardsResponse.isSuccess()) {
                    Log.d("Controller", "Obtaing cards from netowrk");
                    Log.d("Controller", "Remaining cards in online deck: " + cardsResponse.getRemaining());
                    mRemaingCardsOnline = cardsResponse.getRemaining();

                    if (cardsResponse.getRemaining() < CARDS_COUNT) {
                        mDeckApi.drawCards(mDeckId, CARDS_COUNT).compose(ThreadsTransformer::newThreadToMainThread).subscribe(cardsResponse1 -> {
                            Log.d("Controller", "Remaining cards in online deck: " + cardsResponse1.getRemaining());
                            mDiscardedCards.addAll(cardsResponse1.getCards());
                            mRemaingCardsOnline = 0;
                        });
                    }

                    mDrawnCards.addAll(cardsResponse.getCards());

                } else {
                    if (mReshuffledDeck.size() < CARDS_COUNT) {
                        Observable.fromCallable(() -> {
                            // talia online si� ko�czy, wi�c odk�adam jej resztki na deck offline i go tasuj�.
                            Log.d("Controller", "Shuffling");
                            mReshuffledDeck.addAll(cardsResponse.getCards());
                            mReshuffledDeck.addAll(mDiscardedCards);
                            mDiscardedCards.clear();
                            long seed = System.nanoTime();
                            Collections.shuffle(mReshuffledDeck, new Random(seed));
                            Log.d("Controller", "Shuffled, on local Deck");
                            return Observable.empty();
                        }).subscribeOn(Schedulers.newThread()).subscribe(observable -> drawFromLocalDeck());
                    } else {
                        drawFromLocalDeck();
                    }
                }
                findAvaibleCombinations();
            }, Throwable::printStackTrace);
        } else if (isViewPresent()) {
            mView.showToast("Brak wygenerowanej talii");
            mView.hideSpinner();
        }
    }

    /*
    metoda obs�uguj�ca dobieranie kart z lokalnego decku
     */
    private void drawFromLocalDeck() {
        List<Card> cardsFromLocalDeck = Stream.of(mReshuffledDeck).limit(CARDS_COUNT).toList();
        mReshuffledDeck.removeAll(cardsFromLocalDeck);
        mDrawnCards.addAll(cardsFromLocalDeck);
        Log.d("Controller", "Remaiming card in local deck: " + mReshuffledDeck.size());
    }

    /*
    metoda szukaj�ca dost�pnych w�r�d kart kombinacji
     */
    private void findAvaibleCombinations() {
        mFoundedCombinations.clear();
        Observable.fromCallable(() -> {
            checkColorCombinations();
            checkSiblingsCombinations();
            checkFiguresCombinations();
            checkStepsCombinations();
            return Observable.empty();
        }).compose(ThreadsTransformer::newThreadToMainThread).subscribe(objectObservable -> {
            mDrawnCardsSubject.onNext(mDrawnCards);
            mCombinationsSubject.onNext(mFoundedCombinations);
        }, Throwable::printStackTrace);
    }

    /*
    Metoda sprawdza dost�pno�� kombinacji min. 3 takich samych kolor�w.
    Wyci�gni�te karty s� grupowane wg koloru, je�li liczebno�c grupy jest wi�ksza ni� 3 zachodzi warunek istnienia kombinacji
     */
    private void checkColorCombinations() {
        Stream.of(mDrawnCards).groupBy(Card::getSuit).forEach(suitGroup -> {
            if (suitGroup.getValue().size() >= MIN_CARDS_FOR_SET) {
                mFoundedCombinations.add(new Combination(Combination.KOLOR, suitGroup.getValue()));
            }
        });
    }

    /*
   Metoda sprawdza dost�pno�� kombinacji min. 3 takich samych warto�ci kart.
   Wyci�gni�te karty s� grupowane wg warto�ci, je�li liczebno�c grupy jest wi�ksza ni� 3 zachodzi warunek istnienia kombinacji
    */
    private void checkSiblingsCombinations() {
        Stream.of(mDrawnCards).groupBy(Card::getCardValue).forEach(valueGroup -> {
            if (valueGroup.getValue().size() >= MIN_CARDS_FOR_SET) {
                mFoundedCombinations.add(new Combination(Combination.BLIZNIAKI, valueGroup.getValue()));
            }
        });
    }

    /*
    Metoda sprawdza dost�pno�� kombinacji min. 3 figur.
    Wyci�gni�te karty s� filtrowane ze wzgl�du czy s� figur�, je�li liczebno�c wyfiltrowanej grupy jest wi�ksza ni� 3 zachodzi warunek istnienia kombinacji
     */
    private void checkFiguresCombinations() {
        List<Card> figureCards = Stream.of(mDrawnCards).filter(card -> card.getCardValue().equals(Card.DAMA) || card.getCardValue().equals(Card.KROL) || card.getCardValue().equals(Card.WALET)).toList();
        if (figureCards.size() >= MIN_CARDS_FOR_SET) {
            mFoundedCombinations.add(new Combination(Combination.FIGURY, figureCards));
        }
    }

    /*
    Metoda sprawdza dost�pno�� kombinacji schodkowej (min 3 karty po kolei).
    Wyci�gni�te karty s� grupowane wg koloru, je�li liczebno�c grupy jest wi�ksza ni� 3 zachodzi warunek istnienia kombinacji
    W obecnej wersji jest to uproszczony mechanizm znajduj�cy wy��cznie jedn� kombinacj� schodkow� ignoruj�c powt�rki kart jako warto�ci.
     */
    private void checkStepsCombinations() {
        List<Card> mappedCards = Stream.of(mDrawnCards).map(card -> {
            switch (card.getCardValue()) {
                case Card.ACE:
                    card.setCardValue("1");
                    break;
                case Card.DAMA:
                    card.setCardValue("12");
                    break;
                case Card.KROL:
                    card.setCardValue("13");
                    break;
                case Card.WALET:
                    card.setCardValue("11");
                    break;
            }
            return card;
        }).sortBy(card -> Integer.parseInt(card.getCardValue())).toList();

        List<Card> sequentialCards = new ArrayList<>();
        for (int i = 0; i < mappedCards.size(); i++) {
            int cardValue = Integer.parseInt(mappedCards.get(i).getCardValue());
            int nextCardValue = mappedCards.size() > i + 1 ? Integer.parseInt(mappedCards.get(i + 1).getCardValue()) : 0;
            int lastCardInSequenceValue = sequentialCards.isEmpty() ? -2 : Integer.valueOf(sequentialCards.get(sequentialCards.size() - 1).getCardValue());

            if (cardValue + 1 == nextCardValue) {
                //karty s� po kolei
                sequentialCards.add(mappedCards.get(i));
            } else if (sequentialCards.size() >= 2 && lastCardInSequenceValue + 1 == cardValue) {
                sequentialCards.add(mappedCards.get(i));
            } else if (sequentialCards.size() < MIN_CARDS_FOR_SET && cardValue != nextCardValue) {
                sequentialCards.clear();
            }
        }

        if (sequentialCards.size() >= MIN_CARDS_FOR_SET) {
            mFoundedCombinations.add(new Combination(Combination.SCHODKI, sequentialCards));
        }
    }

    /**
     * Metoda pomocnicza do sprawdzenia obecno�ci widoku do operacji na nim
     *
     * @return zmienna mView jest ustawiona
     */
    private boolean isViewPresent() {
        return mView != null;
    }

    /**
     * Metoda pomocnicza do tworzenia tekstu z informacj� o odnalezionych kombinacjach
     *
     * @param combinationList lista odnalezionych kombinacji
     * @return ci�g tekstowy w postaci "nazwa_kombinacji  wartosc_karty kolor  wartosc_karty kolor
     * wartosc_karty kolor  "
     */
    String getCombinationsText(List<Combination> combinationList) {
        //display combinations on screen
        if (combinationList.isEmpty()) {
            return "";
        } else {
            StringBuilder sb = new StringBuilder();
            for (Combination combination : combinationList) {
                sb.append(combination.getNazwa());
                sb.append("  ");
                sb.append(cardListToString(combination.getKarty()));
                sb.append("\n");
            }
            return sb.toString();
        }
    }

    /**
     * Metoda pomocnicza do tworzenia ci�gu tekstowego z listy kart
     *
     * @param cards lista kart do zamiany na tekst
     * @return ci�g tekstowy w postaci "wartosc_karty kolor  "
     */
    private String cardListToString(List<Card> cards) {
        StringBuilder sb = new StringBuilder();
        for (Card card : cards) {
            sb.append(mapIntegerValueToFigure(card.getCardValue()));
            sb.append(" ");
            sb.append(card.getSuit());
            sb.append("   ");
        }
        return sb.toString();
    }

    /**
     * Metoda pomocnicza do zmiany warto�ci liczbowych figur na ich nazwy.
     *
     * @param cardValue warto�� liczbowa karty jako ci�g znak�w
     * @return nazwa figury je�li to figura, w przeciwnym wypadku zwraca @cardValue
     */
    private String mapIntegerValueToFigure(String cardValue) {
        switch (cardValue) {
            case "1":
                return Card.ACE;
            case "11":
                return Card.WALET;
            case "12":
                return Card.DAMA;
            case "13":
                return Card.KROL;
            default:
                return cardValue;
        }
    }

    /**
     * Metoda do subskrypcji na wyci�gni�te karty
     *
     * @param callback akcja do wykonania po zaj�ciu emisji element�w onNext
     * @return disposable
     */
    Disposable subscribeToNewCard(Consumer<List<Card>> callback) {
        return mDrawnCardsSubject != null ? mDrawnCardsSubject.subscribeOn(AndroidSchedulers.mainThread()).subscribe(callback) : null;
    }

    /**
     * Metoda do subskrypcji na odnalezione kombinacje
     *
     * @param callback akcja do wykonania po zaj�ciu emisji element�w onNext
     * @return disposable
     */
    Disposable subscribeToCombinations(Consumer<List<Combination>> callback) {
        return mCombinationsSubject != null ? mCombinationsSubject.subscribeOn(AndroidSchedulers.mainThread()).subscribe(callback) : null;
    }
}
